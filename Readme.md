# JOC DE ROL

#### By Domingo Tarrazona Peris

_Este "juego" se basa en una lucha a muerte, se juegan partidas con un mínimo de 2 jugadores y el último en pié es el
ganador._

## Información 🔰

_⚠️Este proyecto está creado desde 0 por mí, siguiendo las indicaciones facilitadas para completar la práctica haciendo
uso de los recursos aprendidos y de otros nuevos._

### Requisitos 📋

Java 11/+

#### Información Extra de Funcionalidades ✏️

En el apartado de las batallas, en el método hit de player, en caso de que el ataque final sea menor a 0, el ataque
final se establecerá como 1 para evitar búcles infinitos en las partidas.

### Ubicaciones 📦

- Todos los archivos de almacenamiento se guardarán en la ruta `src/almacenamiento/`
- El javadoc se almacenará en la ruta `src/documentación/`
- El archivo jar del juego en la ruta `src/jar/`
- El código se almacenará dentro de la ruta `src/main/java`
