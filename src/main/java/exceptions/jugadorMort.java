package exceptions;

/**
 * Excepción que se lanzará cuando un jugador esté muerto e intente atacar/defender
 */
public class jugadorMort extends Exception {
  public jugadorMort(String msg) {
    super(msg);
  }
}
