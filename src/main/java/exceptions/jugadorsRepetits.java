package exceptions;

/**
 * Excepción que se lanzará cuando un jugador ya esté en un equipo
 */
public class jugadorsRepetits extends Exception {
  public jugadorsRepetits(String msg) {
    super(msg);
  }
}
