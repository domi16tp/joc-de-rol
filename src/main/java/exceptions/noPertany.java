package exceptions;

/**
 * Excepción que se lanzará al intentar borrar una persona de un equipo en el que no está
 */
public class noPertany extends Exception {
  public noPertany(String msg) {
    super(msg);
  }
}
