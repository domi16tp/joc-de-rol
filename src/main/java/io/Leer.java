package io;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase leer:
 * Lee líneas y las convierte a diferentes
 * formatos usando BufferedReader
 */
public class Leer {
  private final static BufferedReader entradaConsola = new BufferedReader(new java.io.InputStreamReader(System.in));

  /**
   * Lee lineas de texto
   *
   * @param mensaje Mensaje que se le muestra al usuario en el terminal
   * @return texto
   */
  public static String leerTexto(String mensaje) {
    String respuesta = null;
    do {
      try {
        System.out.print(mensaje);
        respuesta = entradaConsola.readLine();
      } catch (IOException ex) {
        return "";
      }
    } while (respuesta == null);
    return respuesta;
  }

  /**
   * Lee caracteres
   *
   * @param mensaje Mensaje que se le muestra al usuario en el terminal
   * @return caracter
   */
  public static char leerChar(String mensaje) {
    char respuesta = ' ';
    do {
      try {
        System.out.print(mensaje);
        respuesta = (char) entradaConsola.read();
      } catch (IOException ex) {
        return ' ';
      }
    } while (respuesta == ' ');
    return respuesta;
  }

  /**
   * Lee números enteros
   *
   * @param mensaje Mensaje que se le muestra al usuario en el terminal
   * @return entero
   */
  public static int leerEntero(String mensaje) {
    int n = 0;
    boolean correcto = false;
    while (!correcto) {
      try {
        n = Integer.parseInt(leerTexto(mensaje));
        correcto = true;
      } catch (NumberFormatException ex) {
        System.out.println("Tienes que introducir un número correcto");
      }
    }
    return n;
  }

  /**
   * Lee números double
   *
   * @param mensaje Mensaje que se le muestra al usuario en el terminal
   * @return double
   */
  public static double leerDouble(String mensaje) {
    double n = 0.0;
    boolean correcto = false;
    while (!correcto) {
      try {
        n = Double.parseDouble(leerTexto(mensaje));
        correcto = true;
      } catch (NumberFormatException ex) {
        System.out.println("Tienes que introducir un número correcto");
      }
    }
    return n;
  }

  /**
   * Lee números float
   *
   * @param mensaje Mensaje que se le muestra al usuario en el terminal
   * @return float
   */
  public static float leerFloat(String mensaje) {
    float n = 0;
    boolean correcto = false;
    while (!correcto) {
      try {
        n = Float.parseFloat(leerTexto(mensaje));
        correcto = true;
      } catch (NumberFormatException ex) {
        System.out.println("Tienes que introducir un número correcto");
      }
    }
    return n;
  }

  /**
   * Lee fechas
   *
   * @param mensaje Mensaje que se le muestra al usuario en el terminal
   * @return date
   */
  public static Date leerFecha(String mensaje) {
    Date fecha = new Date();
    DateFormat formatar = new SimpleDateFormat("dd/MM/yyyy");
    boolean correcto = false;
    while (!correcto) {
      try {
        String entrada = leerTexto("Formato DD/MM/YYYY: ");
        fecha = formatar.parse(entrada);
        correcto = true;
      } catch (NumberFormatException | ParseException ex) {
        System.out.println("Tienes que introducir una fecha correcta.");
      }
    }
    return fecha;
  }

  /**
   * Lee boolean
   *
   * @param mensaje Mensaje que se le muestra al usuario en el terminal
   * @return boolean
   */
  public static boolean leerBoolean(String mensaje) {
    boolean n = false;
    n = Boolean.parseBoolean(leerTexto(mensaje));
    return n;
  }
}
