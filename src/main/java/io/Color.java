package io;

/**
 * Clase Color:
 * Contiene colores para cambiar
 * las líneas de texto del terminal
 */
public class Color {
  /**
   * Reinicia el color
   */
  public static final String REINICIAR = "\u001B[0m";
  /**
   * Color negro
   */
  public static final String NEGRO = "\u001B[30m";
  /**
   * Color rojo
   */
  public static final String ROJO = "\u001B[31m";
  /**
   * Color verde
   */
  public static final String VERDE = "\u001B[32m";
  /**
   * Color amarillo
   */
  public static final String AMARILLO = "\u001B[33m";
  /**
   * Color azul
   */
  public static final String AZUL = "\u001B[34m";
  /**
   * Color morado
   */
  public static final String MORADO = "\u001B[35m";
  /**
   * Color cyan
   */
  public static final String CYAN = "\u001B[36m";
  /**
   * Color blanco
   */
  public static final String BLANCO = "\u001B[37m";

  /**
   * Muestra un mensaje de error en el terminal
   *
   * @param msg mensaje
   */
  public static void ERROR(String msg) {
    System.out.println(ROJO + "[×] " + AMARILLO + msg + REINICIAR);
  }

  /**
   * Muestra un mensaje de correcto en el terminal
   *
   * @param msg mensaje
   */
  public static void CORRECTO(String msg) {
    System.out.println(VERDE + "[✓] " + MORADO + msg + REINICIAR);
  }

  /**
   * Genera y muestra un mensaje por el terminal indicando el ganador
   *
   * @param msg mensaje
   */
  public static void GANADOR(String msg) {
    System.out.println(AMARILLO + "[★] " + VERDE + msg + REINICIAR);
  }

  /**
   * Muestra un mensaje en el terminal
   *
   * @param msg mensaje
   */
  public static void MENSAJE(String msg) {
    System.out.println(CYAN + "[✎] " + AZUL + msg + REINICIAR);
  }

  /**
   * Muestra un mensaje indicando que un jugador ha muerto en el terminal
   *
   * @param msg mensaje
   */
  public static void MUERTO(String msg) {
    System.out.println(AMARILLO + "[-] " + ROJO + msg + REINICIAR);
  }

  /**
   * Muestra un mensaje informativo en el terminal
   *
   * @param msg mensaje
   */
  public static void INFO(String msg) {
    System.out.println(BLANCO + "[ⓘ] " + CYAN + msg + REINICIAR);
  }

}
