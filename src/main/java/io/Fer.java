package io;

public class Fer {
    public static void linia(int n){
        for (int i = 0; i < n; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

    public static void liniadoble(int n){
        for(int i=0; i<n; i++)
            System.out.print("=");
        System.out.println();
    }

    public static void liniaart(int n){
        for (int i = 0; i < n; i++) {
            System.out.print("*");
        }
        System.out.println();
    }

    public static boolean isNoZero(int n){
        return n!=0;
    }
}