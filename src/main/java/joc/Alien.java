package joc;

import exceptions.autoatac;
import exceptions.jugadorMort;
import io.Color;

public class Alien extends Player {

  /**
   * Constructor de alien
   *
   * @param name          nombre
   * @param attackPoints  puntos de ataque
   * @param defensePoints puntos de defensa
   * @param life          puntos de vida
   */
  public Alien(String name, int attackPoints, int defensePoints, int life) {
    super(name, attackPoints, defensePoints, life);
  }

  /**
   * Ataques entre jugadores:
   * Un jugador ataca a otro y en caso
   * de que el atacado siga con vida
   * contraatacará.
   * El alien enloquece si tiene más
   * de 20 puntos de vida y gana
   * +3 puntos de ataque y pierde
   * -3 puntos de defensa permanentemente
   *
   * @param p Player
   * @throws jugadorMort
   * @throws autoatac
   */
  @Override
  public void attack(Player p) throws jugadorMort, autoatac {
    System.out.println("\n"+Color.BLANCO+this.getName()+" VS "+p.getName()+Color.REINICIAR+"\n");
    if (this.getLife() <= 0) {
      throw new jugadorMort("El jugador "+ Color.ROJO+this.getName()+Color.AMARILLO+" está muerto y no puede luchar."+Color.REINICIAR);
    }
    if (p.getLife() <= 0) {
      throw new jugadorMort("El jugador "+Color.ROJO+p.getName()+Color.AMARILLO+" está muerto y no puede luchar."+Color.REINICIAR);
    }
    if (this.equals(p)){
      throw new autoatac("El jugador no puede atacarse a si mismo");
    }Color.MENSAJE("ESTADISTICAS ANTES DEL COMBATE");
    System.out.println("Atacant: " + this);
    System.out.println("Atacat: " + p);
    if (this.getLife() > 20) {
      this.setAttackPoints(this.getDefensePoints() + 3);
      this.setDefensePoints(this.getDefensePoints() - 3);
    }
    int ataqueT = this.getAttackPoints();
    int ataqueP = p.getAttackPoints();
    if (this.getItems().size() > 0) {
      for (Item i : this.getItems()) {
        ataqueT += i.getAttackBonus();
      }
    }
    if (p.getItems().size() > 0) {
      for (Item i : p.getItems()) {
        ataqueP += i.getAttackBonus();
      }
    }
    Color.MENSAJE("- COMBATE -");
    p.hit(ataqueT);
    if (p.getLife() > 0) {
      this.hit(ataqueP);
    } else {
      throw new jugadorMort("El jugador "+p.getName()+" está muerto y no puede contraatacar");
    }
    System.out.println();
    Color.MENSAJE("ESTADISTICAS DESPUÉS DEL COMBATE");
    System.out.println("Atacant: " + this);
    System.out.println("Atacat: " + p);
  }
}
