package joc;

import exceptions.jugadorsRepetits;
import exceptions.noPertany;

import java.io.Serializable;
import java.util.ArrayList;

public class Team implements Serializable {
  private String name;
  private ArrayList<Player> players;

  /**
   * Constructor de equipos
   *
   * @param name nombre del equipo
   */
  public Team(String name) {
    this.name = name;
    players = new ArrayList<>();
  }

  /**
   * Añadir un jugador al equipo
   *
   * @param p Player
   * @throws jugadorsRepetits
   */
  public void add(Player p) throws jugadorsRepetits {
    if (this.players.contains(p)){
      throw new jugadorsRepetits("Ese jugador ya se encuentra en el equipo");
    }
    this.players.add(p);
    if (!p.getTeams().contains(this)) {
      p.add(this);
    }
  }

  /**
   * Quitar un jugador del equipo
   *
   * @param p Player
   * @throws noPertany
   */
  public void remove(Player p) throws noPertany {
    if (this.players.contains(p)){
      throw new noPertany("El jugador no pertenece a este equipo");
    }
    this.players.remove(p);
    if (p.getTeams().contains(this)) {
      p.remove(this);
    }
  }

  /**
   * Obtener el ArrayList de jugadores del equipo
   *
   * @return ArrayList de jugadores
   */
  public ArrayList<Player> getMembers() {
    return players;
  }

  /**
   * Comprueba si dos equipos son iguales
   *
   * @param t Team
   * @return true si son iguales o false si son diferentes
   */
  public boolean equals(Team t) {
    if (this.name.compareTo(t.name) == 0 && this.players == t.players) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    String cadena = "";
    for (Player p : players) {
      cadena += "    " + p + "\n";
    }
    return "Equip " + this.name + ":\n" + cadena;
  }

  public String getName() {
    return name;
  }
}
