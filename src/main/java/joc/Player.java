package joc;

import exceptions.autoatac;
import exceptions.jugadorMort;
import exceptions.jugadorsRepetits;
import io.Color;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class Player implements Serializable {
  private String name;
  private int attackPoints;
  private int defensePoints;
  private int life;
  private ArrayList<Team> teams;
  private ArrayList<Item> items;

  /**
   * Constructor de jugadores
   *
   * @param name          Nombre del jugador
   * @param attackPoints  Puntos de ataque del jugador
   * @param defensePoints Puntos de defensa del jugador
   * @param life          Vida del jugador
   */
  public Player(String name, int attackPoints, int defensePoints, int life) {
    this.name = name;
    this.attackPoints = attackPoints;
    this.defensePoints = defensePoints;
    this.life = life;
    teams = new ArrayList<>();
    items = new ArrayList<>();
  }

  /**
   * Ataques entre jugadores:
   * Un jugador ataca a otro y en caso
   * de que el atacado siga con vida
   * contraatacará.
   *
   * @param p Player
   */
  public void attack(Player p) throws jugadorMort, autoatac {
    System.out.println("\n" + Color.BLANCO + this.getName() + " VS " + p.getName() + Color.REINICIAR + "\n");
    if (this.getLife() <= 0) {
      throw new jugadorMort("El jugador " + Color.ROJO + this.getName() + Color.AMARILLO + " está muerto y no puede luchar." + Color.REINICIAR);
    }
    if (p.getLife() <= 0) {
      throw new jugadorMort("El jugador " + Color.ROJO + p.getName() + Color.AMARILLO + " está muerto y no puede luchar." + Color.REINICIAR);
    }
    if (this.equals(p)) {
      throw new autoatac("El jugador no puede atacarse a si mismo");
    }
    int ataqueT = this.getAttackPoints();
    int ataqueP = p.getAttackPoints();
    Color.MENSAJE("ESTADISTICAS ANTES DEL COMBATE");
    System.out.println("Atacant: " + this);
    System.out.println("Atacat: " + p);
    if (this.items.size() > 0) {
      for (Item i : this.items) {
        ataqueT += i.getAttackBonus();
      }
    }
    if (p.items.size() > 0) {
      for (Item i : p.items) {
        ataqueP += i.getAttackBonus();
      }
    }
    Color.MENSAJE("- COMBATE -");
    p.hit(ataqueT);
    if (p.getLife() > 0) {
      this.hit(ataqueP);
    } else {
      throw new jugadorMort("El jugador " + p.getName() + " está muerto y no puede contraatacar");
    }
    System.out.println();
    Color.MENSAJE("ESTADISTICAS DESPUÉS DEL COMBATE");
    System.out.println("Atacant: " + this);
    System.out.println("Atacat: " + p);
  }

  /**
   * Defensa del jugador:
   * Se obtiene los puntos de defensa,
   * las bonificaciones y su vida
   * y se calcula su vida final
   *
   * @param attackPoints Daño del atacante
   */
  protected void hit(int attackPoints) {
    int defensa = this.getDefensePoints();
    int vida = this.getLife();

    for (Item i : this.items) {
      defensa += i.getDefenseBonus();
    }

    int ataqueFinal = attackPoints - defensa;
    if (ataqueFinal < 0) {
      ataqueFinal = 1;
    }
    int vidaFinal = vida - ataqueFinal;
    if (vidaFinal <= 0) {
      System.out.println(this.name + " és colpejat amb " + attackPoints + " punts i es defén amb " + defensa + ". Vides: " + vida + " - " + ataqueFinal + " = 0");
      this.life = 0;
    } else {
      System.out.println(this.name + " és colpejat amb " + attackPoints + " punts i es defén amb " + defensa + ". Vides: " + vida + " - " + ataqueFinal + " = " + vidaFinal);
      this.life = vidaFinal;
    }
  }

  /**
   * Añade un equipo al jugador
   *
   * @param t Team
   * @throws jugadorsRepetits
   */
  public void add(Team t) throws jugadorsRepetits {
    if (this.teams.contains(t)) {
      throw new jugadorsRepetits("Este jugador ya está en el equipo");
    }
    this.teams.add(t);
    if (!t.getMembers().contains(this)) {
      t.add(this);
    }
  }

  /**
   * Elimina un equipo del jugador
   *
   * @param t Team
   */
  public void remove(Team t) {
    this.teams.remove(t);
    if (t.getMembers().contains(this)) {
      try {
        t.remove(this);
      } catch (exceptions.noPertany noPertany) {
        Color.ERROR(noPertany.toString());
      }
    }
  }

  /**
   * Comprobar si un jugador es igual a otro
   *
   * @param p Player
   * @return true si es igual o false si no lo es
   */
  public boolean equals(Player p) {
    if (this.name.compareTo(p.getName()) == 0 && this.attackPoints == p.getAttackPoints() && this.defensePoints == p.getDefensePoints() && this.life == p.getLife()) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Añade un objeto al jugador
   *
   * @param i Item
   */
  public void add(Item i) {
    this.items.add(i);
  }

  /**
   * Quita un objeto al jugador
   *
   * @param i Item
   */
  public void remove(Item i) {
    this.items.remove(i);
  }

  /**
   * Obtener el nombre del jugador
   *
   * @return nombre del jugador
   */
  public String getName() {
    return name;
  }

  /**
   * Obtener los puntos de ataque del jugador
   *
   * @return puntos de ataque del jugador
   */
  public int getAttackPoints() {
    return attackPoints;
  }

  /**
   * Obtener los puntos de defensa del jugador
   *
   * @return puntos de defensa del jugador
   */
  public int getDefensePoints() {
    return defensePoints;
  }

  /**
   * Obtener los puntos de vida del jugador
   *
   * @return puntos de vida del jugador
   */
  public int getLife() {
    return life;
  }

  /**
   * Obtener el ArrayList de equipos del jugador
   *
   * @return ArrayList de equipos
   */
  public ArrayList<Team> getTeams() {
    return teams;
  }

  /**
   * Establecer la vida del jugador
   *
   * @param life vida
   */
  public void setLife(int life) {
    this.life = life;
  }

  /**
   * Establecer los puntos de ataque del jugador
   *
   * @param attackPoints puntos de ataque
   */
  public void setAttackPoints(int attackPoints) {
    this.attackPoints = attackPoints;
  }

  /**
   * Establecer los puntos de defensa del jugador
   *
   * @param defensePoints puntos de defensa
   */
  public void setDefensePoints(int defensePoints) {
    this.defensePoints = defensePoints;
  }

  /**
   * Obtener el ArrayList de objetos del jugador
   *
   * @return ArrayList de objetos
   */
  public ArrayList<Item> getItems() {
    return items;
  }

  public boolean isAlive() {
    if (this.getLife() > 0) {
      return true;
    }
    return false;
  }

  @Override
  public String toString() {
    String StItems = "";
    for (Item it : this.items) {
      StItems += "- " + it + "\n";
    }
    if (this.items.size() == 0) {
      StItems = "- No tiene Items";
    }
    if (this.teams.size() == 1) {
      return this.name + " PA:" + this.attackPoints + " / PD:" + this.defensePoints + " / PV:" + this.life + " (pertany a " + this.teams.size() + " equip) té els ítems:\n" + StItems;
    }
    return this.name + " PA:" + this.attackPoints + " / PD:" + this.defensePoints + " / PV:" + this.life + " (pertany a " + this.teams.size() + " equips) té els ítems:\n" + StItems;
  }
}
