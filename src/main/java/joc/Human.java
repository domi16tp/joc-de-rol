package joc;

public class Human extends Player {
  /**
   * Constructor de human:
   * En caso de que la vida del humano
   * sea superior a 100 se le establecerá
   * una vida de 100 y en caso de que sea
   * menor a 1 se le establecerá 1.
   *
   * @param name          nombre
   * @param attackPoints  puntos de ataque
   * @param defensePoints puntos de defensa
   * @param life          puntos de vida
   */
  public Human(String name, int attackPoints, int defensePoints, int life) {
    super(name, attackPoints, defensePoints, life);
    if (life > 100) {
      super.setLife(100);
    } else if (life < 1) {
      super.setLife(1);
    }
  }

}
