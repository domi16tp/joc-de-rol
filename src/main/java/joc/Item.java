package joc;

import java.io.Serializable;

public class Item implements Serializable {
  private String name;
  private int attackBonus;
  private int defenseBonus;

  /**
   * Constructor de objetos
   *
   * @param name         nombre del objeto
   * @param attackBonus  mejora de ataque
   * @param defenseBonus mejora de defensa
   */
  public Item(String name, int attackBonus, int defenseBonus) {
    this.name = name;
    this.attackBonus = attackBonus;
    this.defenseBonus = defenseBonus;
  }

  /**
   * Devuelve los datos de un objeto
   * @return datos
   */
  @Override
  public String toString() {
    return this.name + " BA:" + this.attackBonus + " / BD:" + this.defenseBonus;
  }

  /**
   * Obtener los puntos de mejora de ataque del objeto
   *
   * @return puntos de mejora de ataque
   */
  public int getAttackBonus() {
    return attackBonus;
  }

  /**
   * Obtener el nombre del objeto
   *
   * @return nombre
   */
  public String getName() {
    return name;
  }

  /**
   * Obtener los puntos de mejora de defensa del objeto
   *
   * @return puntos de mejora de defensa
   */
  public int getDefenseBonus() {
    return defenseBonus;
  }
}
