package joc;

public class Warrior extends Human {

  /**
   * Constructor de Warrior
   *
   * @param name          nombre
   * @param attackPoints  puntos de ataque
   * @param defensePoints puntos de defensa
   * @param life          puntos de vida
   */
  public Warrior(String name, int attackPoints, int defensePoints, int life) {
    super(name, attackPoints, defensePoints, life);
    super.setLife(life);
  }

  /**
   * Defensa del warrior:
   * Se obtiene los puntos de defensa,
   * las bonificaciones y su vida
   * y se calcula su vida final.
   * En caso de que el ataque final recibido
   * sea menor a 5 se omite y no recibe
   * daño.
   *
   * @param attackPoints Daño del atacante
   */
  @Override
  protected void hit(int attackPoints) {
    int defensa = this.getDefensePoints();
    int vida = this.getLife();
    for (Item i : this.getItems()) {
      defensa += i.getDefenseBonus();
    }
    int ataqueFinal = attackPoints - defensa;
    if (ataqueFinal < 5) {
      ataqueFinal = 0;
    }
    int vidaFinal = vida - ataqueFinal;
    if (vidaFinal < 0) {
      System.out.println(this.getName() + " és colpejat amb " + attackPoints + " punts i es defén amb " + defensa + ". Vides: " + vida + " - " + ataqueFinal + " = 0");
      this.setLife(0);
    } else {
      System.out.println(this.getName() + " és colpejat amb " + attackPoints + " punts i es defén amb " + defensa + ". Vides: " + vida + " - " + ataqueFinal + " = " + vidaFinal);
      this.setLife(vidaFinal);
    }
  }
}
