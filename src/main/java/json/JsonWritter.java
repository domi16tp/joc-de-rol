package json;

import com.github.cliftonlabs.json_simple.JsonException;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;
import io.Color;

import java.io.*;

/**
 * Clase para escribir/modificar datos de los archivos de Json
 */
public class JsonWritter {
  /**
   * Generar ficheros iniciales para cada usuario
   *
   * @param almacenamiento ruta de almacenamiento
   * @param n              nombre del jugador
   */
  public static void jsonInit(File almacenamiento, String n) {
    try {
      int vida = 100;
      int defensa = 100;
      String ruta = almacenamiento + "\\" + n + ".json";
      File f = new File(ruta);
      FileWriter fw = new FileWriter(f);
      BufferedWriter bw = new BufferedWriter(fw);

      JsonObject config = new JsonObject();

      config.put("init", true);
      config.put("vida", vida);
      config.put("defensa", defensa);
      config.put("username", n);

      Jsoner.serialize(config, bw);

      bw.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Escribir/Modificar un dato booleano
   *
   * @param almacenamiento ruta de almacenamiento
   * @param nombre         nombre del campo a cambiar
   * @param bool           true/false
   * @param n              nombre del jugador
   */
  public static void jsonWritter(File almacenamiento, String nombre, boolean bool, String n) {
    try {
      String ruta = almacenamiento + "\\" + n + ".json";
      File f = new File(ruta);
      FileReader fr = new FileReader(f);
      BufferedReader br = new BufferedReader(fr);

      JsonObject config = (JsonObject) Jsoner.deserialize(br);

      br.close();
      fr.close();

      FileWriter fw = new FileWriter(f);
      BufferedWriter bw = new BufferedWriter(fw);

      config.replace(nombre, bool);

      Jsoner.serialize(config, bw);

      bw.close();
      fw.close();
    } catch (IOException | JsonException e) {
      e.printStackTrace();
    }
  }

  /**
   * Escribir/Modificar un dato entero
   *
   * @param almacenamiento ruta de almacenamiento
   * @param nombre         nombre del campo a cambiar
   * @param bool           int
   * @param n              nombre del jugador
   */
  public static void jsonWritter(File almacenamiento, String nombre, int bool, String n) {
    try {
      String ruta = almacenamiento + "\\" + n + ".json";
      File f = new File(ruta);
      FileReader fr = new FileReader(f);
      BufferedReader br = new BufferedReader(fr);

      JsonObject config = (JsonObject) Jsoner.deserialize(br);

      br.close();
      fr.close();

      FileWriter fw = new FileWriter(f);
      BufferedWriter bw = new BufferedWriter(fw);

      config.replace(nombre, bool);

      Jsoner.serialize(config, bw);

      bw.close();
      fw.close();
    } catch (IOException | JsonException e) {
      e.printStackTrace();
    }
  }

  /**
   * Escribir/Modificar un dato String
   *
   * @param almacenamiento ruta de almacenamiento
   * @param nombre         nombre del campo a cambiar
   * @param bool           String
   * @param n              nombre del jugador
   */
  public static void jsonWritter(File almacenamiento, String nombre, String bool, String n) {
    try {
      String ruta = almacenamiento + "\\" + n + ".json";
      File f = new File(ruta);
      FileReader fr = new FileReader(f);
      BufferedReader br = new BufferedReader(fr);

      JsonObject config = (JsonObject) Jsoner.deserialize(br);

      br.close();
      fr.close();

      FileWriter fw = new FileWriter(f);
      BufferedWriter bw = new BufferedWriter(fw);

      config.replace(nombre, bool);

      Jsoner.serialize(config, bw);

      bw.close();
      fw.close();
    } catch (IOException | JsonException e) {
      e.printStackTrace();
    }
  }

  /**
   * Establecer el parámetro init
   *
   * @param f ruta
   * @param n nombre
   * @param b booleano
   */
  public static void setInit(File f, String n, boolean b) {
    jsonWritter(f, "init", b, n);
  }

  /**
   * Establecer la vida
   *
   * @param f ruta
   * @param n nombre
   * @param v vida
   */
  public static void setVida(File f, String n, int v) {
    jsonWritter(f, "vida", v, n);
  }

  /**
   * Establecer la defensa
   *
   * @param f ruta
   * @param n nombre
   * @param d defensa
   */
  public static void setDefensa(File f, String n, int d) {
    jsonWritter(f, "defensa", d, n);
  }

  /**
   * Establecer el nombre de usuario
   *
   * @param f ruta
   * @param n nombre
   * @param u usuario
   */
  public static void setUserName(File f, String n, String u) {
    jsonWritter(f, "username", u, n);
  }
}
