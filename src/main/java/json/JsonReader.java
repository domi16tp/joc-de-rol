package json;

import com.github.cliftonlabs.json_simple.JsonException;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;

import java.io.*;

/**
 * Clase para leer los datos de los archivos de Json
 */
public class JsonReader {
  private static boolean b;
  private static String s;
  private static int i;

  /**
   * Leer los campos que se requiera
   *
   * @param almacenamiento ruta
   * @param nombre         campo
   * @param tipo           tipo de datos que se esperan
   * @param n              nombre del archivo
   */
  public static void jsonReader(File almacenamiento, String nombre, String tipo, String n) {
    try {
      String ruta = almacenamiento + "\\" + n + ".json";
      File f = new File(ruta);
      FileReader fr = new FileReader(f);
      BufferedReader br = new BufferedReader(fr);

      JsonObject config = (JsonObject) Jsoner.deserialize(br);

      switch (tipo) {
        case "b":
          b = (boolean) config.get(nombre);
          break;
        case "s":
          s = (String) config.get(nombre);
          break;
        case "i":
          i = (int) config.get(nombre);
          break;
      }

      br.close();
      fr.close();
    } catch (FileNotFoundException | JsonException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Obtener el valor init
   *
   * @param f ruta
   * @param n nombre
   * @return boolean
   */
  public static boolean getInit(File f, String n) {
    jsonReader(f, "init", "b", n);
    return b;
  }

  /**
   * Obtener el valor de vida
   *
   * @param f ruta
   * @param n nombre
   * @return int
   */
  public static int getVida(File f, String n) {
    jsonReader(f, "vida", "i", n);
    return (int) i;
  }

  /**
   * Obtener el valor de la defensa
   *
   * @param f ruta
   * @param n nombre
   * @return int
   */
  public static int getDefensa(File f, String n) {
    jsonReader(f, "defensa", "i", n);
    return i;
  }
}
