package inici;

import io.Color;
import io.Leer;
import joc.*;
import json.JsonReader;
import json.JsonWritter;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * <h2>Joc De Rol</h2>
 * <p>
 * Enlace al repositorio:
 * <a href="https://gitlab.com/domi16tp/joc-de-rol">Repositorio</a>
 *
 * @author Domingo Tarrazona Peris
 * @version 1.0
 */
public class JocDeRol {
  public static int PV = 100;
  public static int PD = 100;
  public static String NombreJugador;
  private static ArrayList<Player> players = new ArrayList<>();
  private static ArrayList<Team> teams = new ArrayList<>();
  private static ArrayList<Item> items = new ArrayList<>();
  private static ArrayList<Player> ranking = new ArrayList<>();

  public static void main(String[] args) {

    onStart();

    boolean menu = true;

    do {
      boolean config = true;
      boolean jugar = true;

      System.out.println(Color.ROJO + "JOC" + Color.AMARILLO + " DE " + Color.ROJO + "ROL" + Color.REINICIAR);
      System.out.println("1. Configuració ");
      System.out.println("2. Jugar (" + Color.VERDE + players.size() + Color.MORADO + " Jugadores Listos" + Color.REINICIAR + ")");
      System.out.println("3. " + Color.ROJO + "Eixir" + Color.REINICIAR);
      int opc = Leer.leerEntero("Selecciona una opción: ");
      switch (opc) {
        case 1:
          do {
            boolean jugadors = true;
            boolean equips = true;
            boolean obj = true;
            System.out.println("\nCONFIGURACIÓ");
            System.out.println("1. Gestió jugadors");
            System.out.println("2. Gestió equips");
            System.out.println("3. Gestió objectes");
            System.out.println("4. Exportar");
            System.out.println("5. Importar");
            System.out.println("6. Eixir");
            opc = Leer.leerEntero("Selecciona una opción: ");
            switch (opc) {
              case 1:
                do {
                  System.out.println("\nJUGADORS");
                  System.out.println("1. Crear jugador");
                  System.out.println("2. Mostrar jugadors");
                  System.out.println("3. Esborrar jugador");
                  System.out.println("4. Assignar jugador a equip");
                  System.out.println("5. Assignar objecte a jugador");
                  System.out.println("6. Cambiar vida máxima");
                  System.out.println("7. Eixir");
                  opc = Leer.leerEntero("Selecciona una opción: ");
                  switch (opc) {
                    case 1:
                      System.out.println("\nCREAR JUGADOR");
                      System.out.println("1. Humà");
                      System.out.println("2. Guerrer");
                      System.out.println("3. Alien");
                      opc = Leer.leerEntero("Selecciona una opción: ");
                      String nom = Leer.leerTexto("Nom: ");
                      int atac = 0;
                      do {
                        atac = Leer.leerEntero("Punts d'atac (1-100): ");
                      } while (atac < 1 || atac > 100);
                      switch (opc) {
                        case 1:
                          Human nouHuma = new Human(nom, atac, PD - atac, PV);
                          if (addPlayer(nouHuma)) {
                            Color.CORRECTO("El jugador se ha creado correctamente.");
                          } else {
                            Color.ERROR("Error al crear al jugador: \n     ¡Ya existe un jugador con ese nombre!");
                          }
                          break;
                        case 2:
                          Warrior nouWarrior = new Warrior(nom, atac, PD - atac, PV);
                          if (addPlayer(nouWarrior)) {
                            Color.CORRECTO("El jugador se ha creado correctamente.");
                          } else {
                            Color.ERROR("Error al crear al jugador: \n     ¡Ya existe un jugador con ese nombre!");
                          }
                          break;
                        case 3:
                          Alien nouAlien = new Alien(nom, atac, PD - atac, PV);
                          if (addPlayer(nouAlien)) {
                            Color.CORRECTO("El jugador se ha creado correctamente.");
                          } else {
                            Color.ERROR("Error al crear al jugador: \n     ¡Ya existe un jugador con ese nombre!");
                          }
                          break;
                      }
                      break;
                    case 2:
                      playerList();
                      break;
                    case 3:
                      System.out.println("\nBORRAR UN JUGADOR");
                      playerList();
                      String nom2 = Leer.leerTexto("Dime el nombre del jugador que quieres borrar: ");
                      if (buscarJugador(nom2)) {
                        Player pl = getJugador(nom2);
                        players.remove(pl); //   Elimino al jugador del arraylist
                        for (Team t : teams) {  // Busco en cada equipo y si contiene al jugador lo borro
                          if (t.getMembers().contains(pl)) {
                            try {
                              t.remove(pl);
                            } catch (exceptions.noPertany noPertany) {
                              Color.ERROR(noPertany.toString());
                            }
                          }
                        }
                        Color.CORRECTO("El jugador se ha borrado correctamente!");
                      } else {
                        Color.ERROR("Jugador no encontrado!");
                      }
                      break;
                    case 4:
                      String nomE = Leer.leerTexto("Dime el nombre del equipo: ");
                      String nom3 = Leer.leerTexto("Dime el nombre del jugador: ");
                      if (buscarEquip(nomE)) {
                        if (buscarJugador(nom3)) {
                          Team eq = getEquip(nomE);
                          try {
                            eq.add(getJugador(nom3));
                          } catch (exceptions.jugadorsRepetits jugadorsRepetits) {
                            Color.ERROR(jugadorsRepetits.toString());
                          }
                          Color.CORRECTO("El equipo ha sido asignado correctamente!");
                        } else {
                          Color.ERROR("Nombre del jugador incorrecto!");
                        }
                      } else {
                        Color.ERROR("Nombre del equipo incorrecto!");
                      }
                      break;
                    case 5:
                      String nomO = Leer.leerTexto("Dime el nombre del objeto: ");
                      String nom4 = Leer.leerTexto("Dime el nombre del jugador: ");
                      if (buscarItem(nomO)) {
                        if (buscarJugador(nom4)) {
                          Player p = getJugador(nom4);
                          p.add(getItem(nomO));
                          Color.CORRECTO("El objeto se ha asignado correctamente!");
                        } else {
                          Color.ERROR("Nombre del jugador incorrecto");
                        }
                      } else {
                        Color.ERROR("Nombre del objeto incorrecto");
                      }
                      break;
                    case 6:
                      int nVida = Leer.leerEntero("Dime los nuevos puntos máximos de vida: ");
                      JsonWritter.setVida(almacenamiento, NombreJugador, nVida);
                      PV = nVida;
                      break;
                    case 7:
                      jugadors = false;
                      break;
                  }
                } while (jugadors);
                break;
              case 2:
                do {
                  System.out.println("\nEQUIPS");
                  System.out.println("1. Crear equip");
                  System.out.println("2. Mostrar equips");
                  System.out.println("3. Esborrar equip");
                  System.out.println("4. Assignar equip a jugador");
                  System.out.println("5. Eixir");
                  opc = Leer.leerEntero("Selecciona una opción: ");
                  switch (opc) {
                    case 1:
                      String nom = Leer.leerTexto("Dime el nombre del equipo: ");
                      Team nouEquip = new Team(nom);
                      if (!buscarEquip(nom)) {
                        if (addTeam(nouEquip)) {
                          Color.CORRECTO("El equipo se ha creado correctamente.");
                        } else {
                          Color.ERROR("Error al crear el equipo!");
                        }
                      } else {
                        Color.ERROR("Error al crear al equipo: \n     ¡Ya existe un equipo con ese nombre!");
                      }
                      break;
                    case 2:
                      teamList();
                      break;
                    case 3:
                      String nomE = Leer.leerTexto("Dime el nombre del equipo: ");
                      if (buscarEquip(nomE)) {
                        Team eq = getEquip(nomE);
                        teams.remove(eq);
                        for (Player p : players) {
                          if (p.getTeams().contains(eq)) {
                            p.remove(eq);
                          }
                        }
                      } else {
                        Color.ERROR("No existe ningún equipo con ese nombre!");
                      }
                      break;
                    case 4:
                      String nomE2 = Leer.leerTexto("Dime el nombre del equipo: ");
                      if (buscarEquip(nomE2)) {
                        String jug = Leer.leerTexto("Dime el nombre del jugador: ");
                        if (buscarJugador(jug)) {
                          Team addJugador = getEquip(nomE2);
                          try {
                            addJugador.add(getJugador(jug));
                          } catch (exceptions.jugadorsRepetits jugadorsRepetits) {
                            Color.ERROR(jugadorsRepetits.toString());
                          }
                        } else {
                          Color.ERROR("El jugador no existe!");
                        }
                      } else {
                        Color.ERROR("El equipo no existe!");
                      }
                      break;
                    case 5:
                      equips = false;
                      break;
                  }
                } while (equips);
                break;
              case 3:
                do {
                  System.out.println("\nOBJECTES");
                  System.out.println("1. Crear objecte");
                  System.out.println("2. Mostrar objectes");
                  System.out.println("3. Esborrar objecte");
                  System.out.println("4. Assignar objecte a jugador");
                  System.out.println("5. Eixir");
                  opc = Leer.leerEntero("Selecciona una opción: ");
                  switch (opc) {
                    case 1:
                      String nomO = Leer.leerTexto("Dime el nombre del objeto que quieres añadir: ");
                      if (buscarItem(nomO)) {
                        Color.ERROR("Ya hay un nombre con ese objeto!");
                      } else {
                        int ab = Leer.leerEntero("Dime la bonificación en ataque: ");
                        int db = Leer.leerEntero("Dime la bonificación en defensa: ");
                        Item i = new Item(nomO, ab, db);
                        if (addItems(i)) {
                          Color.CORRECTO("Objeto creado correctamente!");
                        }
                      }
                      break;
                    case 2:
                      itemList();
                      break;
                    case 3:
                      String nomOb = Leer.leerTexto("Dime el nombre del objeto: ");
                      if (buscarItem(nomOb)) {
                        items.remove(getItem(nomOb));
                        for (Player p : players) {
                          if (p.getItems().contains(getItem(nomOb))) {
                            p.remove(getItem(nomOb));
                            Color.CORRECTO("Objeto eliminado correctamente.");
                          }
                        }
                      } else {
                        Color.ERROR("No existe un objeto con ese nombre!");
                      }
                      break;
                    case 4:
                      String nomI = Leer.leerTexto("Dime el nombre del objeto: ");
                      if (buscarItem(nomI)) {
                        String nomP = Leer.leerTexto("Dime el nombre del jugador: ");
                        if (buscarItem(nomP)) {
                          Player p = getJugador(nomP);
                          p.add(getItem(nomI));
                        } else {
                          Color.ERROR("No existe un jugador con ese nombre!");
                        }
                      } else {
                        Color.ERROR("No existe un objeto con ese nombre!");
                      }
                      break;
                    case 5:
                      obj = false;
                      break;
                  }
                } while (obj);
                break;
              case 4:
                System.out.println("Archivos disponibles:");
                System.out.println("0.- Exportar todo");
                System.out.println("1.- players.dat");
                System.out.println("2.- teams.dat");
                System.out.println("3.- items.dat");
                opc = Leer.leerEntero("Selecciona un archivo: ");
                switch (opc) {
                  case 0:
                    exportar("players.dat", players);
                    exportar("teams.dat", teams);
                    exportar("items.dat", items);
                    break;
                  case 1:
                    exportar("players.dat", players);
                    break;
                  case 2:
                    exportar("teams.dat", teams);
                    break;
                  case 3:
                    exportar("items.dat", items);
                    break;
                  default:
                    Color.ERROR("Opción no válida.");
                }
                break;
              case 5:
                System.out.println("Archivos disponibles:");
                System.out.println("0.- Importar todos");
                System.out.println("1.- players.dat");
                System.out.println("2.- teams.dat");
                System.out.println("3.- items.dat");
                opc = Leer.leerEntero("Selecciona un archivo: ");
                switch (opc) {
                  case 0:
                    importar("players.dat", true);
                    importar("teams.dat", true);
                    importar("items.dat", true);
                    break;
                  case 1:
                    importar("players.dat", true);
                    break;
                  case 2:
                    importar("teams.dat", true);
                    break;
                  case 3:
                    importar("items.dat", true);
                    break;
                  default:
                    Color.ERROR("Opción no válida.");
                }
                break;
              case 6:
                config = false;
                break;
            }
          } while (config);
          break;
        case 2:
          do {

            if (players.size() < 2) {
              Color.INFO("Se necesitan mínimo 2 personas para iniciar la partida.");
              jugar = false;
            } else {
              jugar();

              Color.MENSAJE("La partida ha finalizado!");
              String continuar = Leer.leerTexto("Quieres repetir la partida o salir? (c/s) ");
              if (continuar.equalsIgnoreCase("s")) {
                jugar = false;
              }
            }
          } while (jugar);
          break;
        case 3:
          menu = false;
          break;
        default:
          Color.ERROR("Opción no válida");
      }
    } while (menu);
  }

  /**
   * Añade un jugador a la lista de jugadores
   *
   * @param p Player
   * @return true si se ha añadido, false si ya existe en la lista
   */
  public static boolean addPlayer(Player p) {
    for (Player pl : players) {
      if (p.equals(pl)) {
        return false;
      }
    }
    players.add(p);
    return true;
  }

  /**
   * Añade un equipo a la lista de equipos
   *
   * @param t Team
   * @return true si se ha añadido, false si ya existe en la lista
   */
  public static boolean addTeam(Team t) {
    for (Team te : teams) {
      if (t.equals(te)) {
        return false;
      }
    }
    teams.add(t);
    return true;
  }

  /**
   * Añade un objeto a la lista de objetos
   *
   * @param i Item
   * @return true si se ha añadido, false si ya existe en la lista
   */
  public static boolean addItems(Item i) {
    for (Item it : items) {
      if (i.equals(it)) {
        return false;
      }
    }
    items.add(i);
    return true;
  }

  /**
   * Muestra la lista de jugadores
   */
  public static void playerList() {
    System.out.println("\nLISTA DE JUGADORES");
    for (Player p : players) {
      System.out.println("- " + p);
    }
  }

  /**
   * Muestra la lista de equipos
   */
  public static void teamList() {
    System.out.println("\nLISTA DE EQUIPOS");
    for (Team t : teams) {
      System.out.println("- " + t);
    }
  }

  /**
   * Muestra la lista de objetos
   */
  public static void itemList() {
    System.out.println("\nLISTA DE OBJETOS");
    for (Item i : items) {
      System.out.println("- " + i);
    }
  }

  /**
   * Buscar un jugador en la lista
   *
   * @param nom Nombre del jugador
   * @return true si el jugador está en la lista de jugadores o false si no está
   */
  public static boolean buscarJugador(String nom) {
    for (Player p : players) {
      if (p.getName().equalsIgnoreCase(nom)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Obtener un jugador mediante el nombre
   *
   * @param nom Nombre del jugador
   * @return jugador(Player)
   */
  public static Player getJugador(String nom) {
    for (Player p : players) {
      if (p.getName().equalsIgnoreCase(nom)) {
        return p;
      }
    }
    Color.ERROR("No se ha encontrado a ese jugador.");
    return null;
  }

  /**
   * Buscar un equipo en la lista
   *
   * @param nom Nombre del equipo
   * @return true si el equipo está en la lista o false si no está
   */
  public static boolean buscarEquip(String nom) {
    for (Team t : teams) {
      if (t.getName().equalsIgnoreCase(nom)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Obtener un equipo mediante el nombre
   *
   * @param nom Nombre del equipo
   * @return equipo(Team)
   */
  public static Team getEquip(String nom) {
    for (Team t : teams) {
      if (t.getName().equalsIgnoreCase(nom)) {
        return t;
      }
    }
    Color.ERROR("No se ha encontrado ese equipo.");
    return null;
  }

  /**
   * Buscar un objeto en la lista
   *
   * @param nom Nombre del objeto
   * @return true si el objeto está en la lista o false si no está
   */
  public static boolean buscarItem(String nom) {
    for (Item i : items) {
      if (i.getName().equalsIgnoreCase(nom)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Obtener un objeto mediante el nombre
   *
   * @param nom Nombre del objeto
   * @return objeto(Item)
   */
  public static Item getItem(String nom) {
    for (Item i : items) {
      if (i.getName().equalsIgnoreCase(nom)) {
        return i;
      }
    }
    Color.ERROR("No se ha encontrado ese item.");
    return null;
  }

  /**
   * Inicia la partida
   * La partida es automática, cada jugador
   * de la lista luchará contra otro aleatorio
   * hasta que solo 1 quede vivo.
   */
  public static void jugar() {
    exportar("players.dat", players);
    boolean fin = true;
    while (fin) {
      int vivos = 0;
      for (Player atacante : players) {
        Player defensor;
        do {
          defensor = vs();
        } while (defensor.equals(atacante));
        try {
          atacante.attack(defensor);
          if (!atacante.isAlive()) {
            Color.MUERTO("El jugador " + atacante.getName() + " ha muerto!");
          }
          if (!defensor.isAlive()) {
            Color.MUERTO("El jugador " + defensor.getName() + " ha muerto!");
          }
        } catch (exceptions.jugadorMort jugadorMort) {
          Color.ERROR(jugadorMort.toString());
        } catch (exceptions.autoatac autoatac) {
          Color.ERROR(autoatac.toString());
        }
        timer();
        for (Player player : players) {
          if (player.isAlive()) {
            vivos++;
          }
        }
        if (vivos == 1) {
          Player ganador = null;
          for (Player player : players) {
            if (player.isAlive()) {
              ganador = player;
            }
          }
          Color.GANADOR("El ganador es " + ganador.getName());
          Color.MENSAJE("Ha ganado a " + (players.size() - 1) + " contrincantes.");
          ranking(ganador);
          importar("players.dat", false);
          return;
        }
      }
    }

  }

  /**
   * Genera un jugador aleatorio para la batalla.
   *
   * @return Un jugador aleatorio de la lista
   */
  public static Player vs() {
    int random = (int) (Math.random() * players.size());
    return players.get(random);
  }

  public static File almacenamiento = new File(".\\src\\almacenamiento");

  /**
   * Guardar los datos en archivos
   *
   * @param nombre nombre del archivo
   * @param a      ArrayList que se va a guardar
   */
  public static void exportar(String nombre, ArrayList a) {
    FileOutputStream fos = null;
    try {
      String ruta = almacenamiento + "\\" + nombre;
      File f = new File(ruta);
      fos = new FileOutputStream(f);
      ObjectOutputStream oos = new ObjectOutputStream(fos);

      oos.writeObject(a);

      oos.close();
    } catch (FileNotFoundException e) {
      Color.ERROR(e.toString());
    } catch (IOException e) {
      Color.ERROR(e.toString());
    } finally {
      try {
        fos.close();
      } catch (IOException e) {
        Color.ERROR(e.toString());
      }
    }
  }

  /**
   * Importar los datos de un archivo
   *
   * @param nombre          nombre del archivo
   * @param mostrarMensajes true para que muestre los mensajes o false para que no los muestre
   */
  public static void importar(String nombre, boolean mostrarMensajes) {
    String ruta = almacenamiento + "\\" + nombre;
    File f = new File(ruta);
    if (!f.exists()) {
      if (mostrarMensajes) Color.INFO("El fichero " + nombre + " no está disponible, omitiendo.");
      return;
    }
    try {
      FileInputStream fis = new FileInputStream(f);
      ObjectInputStream ois = new ObjectInputStream(fis);

      switch (nombre) {
        case "players.dat":
          while (fis.available() > 0) {
            players = (ArrayList<Player>) ois.readObject();
            if (mostrarMensajes) Color.CORRECTO("Se han importado correctamente los jugadores!");
          }
          break;
        case "teams.dat":
          while (fis.available() > 0) {
            teams = (ArrayList<Team>) ois.readObject();
            if (mostrarMensajes) Color.CORRECTO("Se han importado correctamente los equipos!");
          }
          break;
        case "items.dat":
          while (fis.available() > 0) {
            items = (ArrayList<Item>) ois.readObject();
            if (mostrarMensajes) Color.CORRECTO("Se han importado correctamente los objetos!");
          }
          break;
        case "ranking.dat":
          while (fis.available() > 0) {
            ranking = (ArrayList<Player>) ois.readObject();
            if (mostrarMensajes) Color.CORRECTO("Se han importado correctamente el ranking!");
          }
          break;
        default:
          if (mostrarMensajes) Color.ERROR("No existe un archivo con ese nombre!");
      }

      ois.close();
      fis.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  /**
   * Genera y actualiza el ranking
   *
   * @param ganador Ganador de la batalla
   */
  public static void ranking(Player ganador) {
    final int max = 5;

    importar("ranking.dat", false);

    ranking.add(ganador);
    Collections.sort(ranking, new CompararVida());

    if (ranking.size() > 5) {
      ranking.remove(5);
    }

    exportar("ranking.dat", ranking);

    Color.INFO("-- RANKING --");
    for (int i = 0; i < ranking.size(); i++) {
      int a = i + 1;
      System.out.println("[" + a + "] " + ranking.get(i).getName());
    }
  }

  /**
   * Temporizador
   */
  public static void timer() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException ex) {
      Thread.currentThread().interrupt();
    }
  }

  /**
   * Importa los datos cada vez que el programa se inicia y genera los archivos y carpetas correspondientes
   */
  public static void onStart() {
    if (!almacenamiento.exists()) {
      if (!almacenamiento.mkdirs()) {
        Color.ERROR("Error al crear el directorio de almacenamiento.");
      } else {
        Color.CORRECTO("Se ha generado correctamente el directorio de almacenamiento.");
      }
    }

    NombreJugador = Leer.leerTexto("Dime tú nombre de usuario: ");
    File j = new File(almacenamiento + "\\" + NombreJugador + ".json");
    if (!j.exists()) {
      JsonWritter.jsonInit(almacenamiento, NombreJugador);
    }
    if (JsonReader.getInit(almacenamiento, NombreJugador)) {
      Color.INFO("Bienvenido " + NombreJugador + " a Juego de Rol");
      System.out.println("");
      System.out.println("Este es el tutorial del juego, se muestra cuando se crea un nuevo usuario.");
      System.out.println("En el apartado de configuración puedes crear jugadores, equipos u objetos");
      System.out.println("y añadirlos a los jugadores.");
      System.out.println("Cuando tengas un mínimo de 2 jugadores, podrás acceder al apartado de jugar.");
      System.out.println("El apartado de jugar funciona de forma aleatoria, cuando inicies la partida");
      System.out.println("empezará a seleccionar jugadores de la lista y los emparejará aleatoriamente");
      System.out.println("con otros jugadores hasta que solo quede 1 con vida.\n");
      Leer.leerTexto("Presiona ENTER para continuar       ");
      JsonWritter.jsonWritter(almacenamiento, "init", false, NombreJugador);
    }

    if (almacenamiento.exists()) {
      Color.MENSAJE("-- IMPORTANDO DATOS --");
      importar("players.dat", true);
      importar("teams.dat", true);
      importar("items.dat", true);
      importar("ranking.dat", true);
    }
    System.out.println("\n\n");
  }
}
